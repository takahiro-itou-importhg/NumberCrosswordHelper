﻿Option Explicit On

Public Class PuzzleField

    Public Structure TCharTable
        Public nNumber As Integer
        Public sChar As String
    End Structure

    Public m_numCols As Integer
    Public m_numRows As Integer
    Public m_nMaxIdx As Integer

    Public m_aryField(,) As Integer
    Public m_aryChars(,) As String
    Public m_recPlay() As TCharTable
    Public m_tblChars() As String

    Public Enum SquareType
        FIELD_SQRTYPE_BLACK = -1
        FIELD_SQRTYPE_NOSELECT = -2
    End Enum

    Public Function readFieldFromFile(ByVal fileName As String) As Boolean

        Dim sr As New System.IO.StreamReader(fileName, System.Text.Encoding.UTF8)

        readFieldFromFile = readFieldFromStream(sr)
        sr.Close()

    End Function

    Public Function readFieldFromStream(ByRef sr As System.IO.StreamReader) As Boolean

        Dim strLine As String
        Dim strTemp As String
        Dim X As Integer, Y As Integer
        Dim nMaxIdx As Integer = 0
        Dim nCurIdx As Integer

        Try
            ' 列数と行数を読み込む。
            strLine = sr.ReadLine()
            strTemp = getNextToken(strLine, ",")
            m_numCols = Integer.Parse(strTemp)

            strTemp = getNextToken(strLine, ",")
            m_numRows = Integer.Parse(strTemp)

            ReDim m_aryField(m_numCols, m_numRows)
            ReDim m_aryChars(m_numCols, m_numRows)

            For Y = 0 To m_numRows - 1
                If (sr.Peek() <= -1) Then
                    MsgBox("データの行数が不足しています。" & vbCrLf _
                        & Y & " 行 / " & m_numRows & " 行読み込み。")
                    Return False
                End If

                strLine = sr.ReadLine()
                For X = 0 To m_numCols - 1
                    strTemp = getNextToken(strLine, ",")
                    If strTemp = "" Then
                        MsgBox("データの列数が不足。" & (Y + 1) & " 行目" & vbCrLf _
                            & X & " 列 / " & m_numCols & " 列読み込み。")
                    End If
                    If (strTemp = "■" Or strTemp = "_") Then
                        m_aryField(X, Y) = SquareType.FIELD_SQRTYPE_BLACK
                        m_aryChars(X, Y) = ""
                        Continue For
                    End If
                    If (Val(strTemp) = 0) Then
                        m_aryField(X, Y) = 0
                        m_aryChars(X, Y) = strTemp
                        Continue For
                    End If

                    nCurIdx = Integer.Parse(strTemp)
                    If (nMaxIdx < nCurIdx) Then nMaxIdx = nCurIdx
                    m_aryField(X, Y) = nCurIdx
                Next
            Next

            ' シグネチャを確認する
            strLine = sr.ReadLine()
            If (strLine <> "END FIELD") Then
                MsgBox("データ終端のシグネチャが見つかりません。データの行数が過剰です。")
                Return False
            End If

            ' 番号の最大値を読み込む
            strLine = sr.ReadLine()
            If (Integer.Parse(strLine) <> nMaxIdx) Then
                MsgBox("番号の最大値が一致しません。" & vbCrLf & _
                       "ファイル記載 = " & Integer.Parse(strLine) & vbCrLf & _
                       "実際の最大値 = " & nMaxIdx)
                Return False
            End If
            m_nMaxIdx = Integer.Parse(strLine)
            ReDim m_tblChars(m_nMaxIdx + 1)
            ReDim m_recPlay(m_nMaxIdx + 1)

            For Y = 0 To m_nMaxIdx - 1
                If (sr.Peek() <= -1) Then
                    MsgBox("予期せぬファイル終端")
                    Return False
                End If

                strLine = sr.ReadLine()
                If (strLine = "END TABLE") Then Exit For
                strTemp = getNextToken(strLine, "=")
                X = Integer.Parse(strTemp)
                With m_recPlay(Y)
                    .nNumber = X
                    .sChar = strLine
                End With
                m_tblChars(X) = strLine
            Next
        Catch
            Return False
        End Try

        Return True

    End Function

    Public Function writeFieldToFile(ByVal fileName As String) As Boolean

        Try
            Dim sw As New System.IO.StreamWriter(fileName, False, System.Text.Encoding.UTF8)
            writeFieldToFile = writeFieldToStream(sw)
            sw.Close()
        Catch ex As Exception
            MsgBox("書き込みエラー")
            Return False
        End Try

    End Function

    Public Function writeFieldToStream(ByRef sw As System.IO.StreamWriter) As Boolean

        Dim strLine As String
        Dim strTemp As String
        Dim X As Integer, Y As Integer
        Dim nCurIdx As Integer

        Try
            ' 列数と行数を書き込む。
            strline = m_numCols.ToString() & "," & m_numRows.ToString() & ","
            sw.WriteLine(strLine)

            For Y = 0 To m_numRows - 1
                strLine = ""
                For X = 0 To m_numCols - 1
                    nCurIdx = m_aryField(X, Y)
                    If (nCurIdx = SquareType.FIELD_SQRTYPE_BLACK) Then
                        strTemp = "■"
                    ElseIf (nCurIdx = 0) Then
                        strTemp = m_aryChars(X, Y)
                    Else
                        strTemp = m_aryField(X, Y).ToString()
                    End If
                    strLine = strLine & strTemp & ","
                Next
                sw.WriteLine(strLine)
            Next

            ' シグネチャを書き込む。
            sw.WriteLine("END FIELD")

            ' 番号の最大値を書き込む。
            strLine = m_nMaxIdx.ToString()
            sw.WriteLine(strLine)

            ' 回答を書き込む
            For Y = 1 To m_nMaxIdx
                If (m_tblChars(Y) = "") Then Continue For
                strLine = (Y).ToString() & "=" & m_tblChars(Y)
                sw.WriteLine(strLine)
            Next
            sw.WriteLine("END TABLE")

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function

End Class
