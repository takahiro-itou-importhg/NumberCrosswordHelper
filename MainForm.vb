﻿Public Class MainForm

    Public m_objField As PuzzleField
    Private m_frmCheckSheet As CheckSheet

    Private m_cxCellWidth As Integer = 32
    Private m_cyCellHeight As Integer = 32
    Private m_curSelNumber As Integer = PuzzleField.SquareType.FIELD_SQRTYPE_NOSELECT

    Public Sub notifyUnloadCheckSheet()
        m_frmCheckSheet = Nothing
    End Sub

    Public Sub changeSelectNumber(ByVal nNewIdx As Integer)
        m_curSelNumber = nNewIdx
        updateField(m_objField)
    End Sub

    Public Function inputCharacter(ByRef objField As PuzzleField, ByRef frmCheck As CheckSheet, ByVal nSelNum As Integer) As Boolean

        Dim strChar As String = InputBox("指定した番号に対応する文字を入力してください。", , objField.m_tblChars(nSelNum))
        Dim I As Integer, X As Integer, Y As Integer
        Dim retMsg As MsgBoxResult

        ' 文字が重複していないか検査する。
        If (strChar <> "") And (strChar <> "■") Then
            For I = 1 To objField.m_nMaxIdx
                If ((I <> nSelNum) And (objField.m_tblChars(I) = strChar)) Then
                    retMsg = MsgBox("その文字はすでに使われています。続行してもよろしいですか？", MsgBoxStyle.OkCancel Or MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question)
                    If (retMsg = MsgBoxResult.Cancel) Then
                        Return False
                    End If
                End If
            Next
            For Y = 0 To objField.m_numRows - 1
                For X = 0 To objField.m_numCols - 1
                    If (objField.m_aryChars(X, Y) = strChar) Then
                        retMsg = MsgBox("その文字はヒントに使われています。続行してもよろしいですか？", MsgBoxStyle.OkCancel Or MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Question)
                        If (retMsg = MsgBoxResult.Cancel) Then
                            Return False
                        End If
                    End If
                Next
            Next
        End If

        objField.m_tblChars(nSelNum) = strChar
        changeSelectNumber(nSelNum)

        If frmCheck IsNot Nothing Then
            frmCheck.updateCheckSheet()
        End If

        Return True
    End Function

    Private Sub initializeField(ByRef objField As PuzzleField)
        Dim numCols As Integer
        Dim numRows As Integer
        Dim sumWidth As Integer
        Dim sumHeight As Integer

        With objField
            numCols = .m_numCols
            numRows = .m_numRows
        End With

        With grdField
            sumWidth = .RowHeadersWidth
            sumHeight = .ColumnHeadersHeight

            .ColumnCount = numCols
            For X = 0 To numCols - 1
                With .Columns(X)
                    .SortMode = DataGridViewColumnSortMode.NotSortable
                    .Width = m_cxCellWidth
                    .Name = (X + 1).ToString()
                    sumWidth = sumWidth + .Width
                End With
            Next

            .RowCount = numRows
            .RowHeadersWidth = 48
            For Y = 0 To numRows - 1
                With .Rows(Y)
                    .Height = m_cyCellHeight
                    .HeaderCell.Value = (Y + 1).ToString()
                    sumHeight = sumHeight + .Height
                End With
            Next
        End With

        With Me
            .Width = sumWidth + 64
            .Height = sumHeight + .mnuMain.Height + 96
        End With
    End Sub

    Private Sub showCheckSheet()
        If (m_frmCheckSheet Is Nothing) Then
            m_frmCheckSheet = New CheckSheet
        End If

        m_frmCheckSheet.setupMainForm(Me)
        m_frmCheckSheet.Show()
    End Sub

    Private Sub updateField(ByRef objField As PuzzleField)

        Dim numCols As Integer
        Dim numRows As Integer
        Dim valTemp As Integer
        Dim strTemp As String

        With objField
            numCols = objField.m_numCols
            numRows = objField.m_numRows
        End With

        With grdField
            Dim fntParent As Font = .Font
            Dim fntHint As Font = New Font(fntParent.Name, 14, fntParent.Style Or FontStyle.Bold)
            Dim fntNumber As Font = New Font(fntParent.Name, 9, FontStyle.Regular)

            For Y = 0 To numRows - 1
                With .Rows(Y)
                    For X = 0 To numCols - 1
                        valTemp = objField.m_aryField(X, Y)
                        With .Cells(X)
                            If (valTemp = PuzzleField.SquareType.FIELD_SQRTYPE_BLACK) Then
                                .Style.BackColor = Color.Black
                                strTemp = ""
                            ElseIf (valTemp = 0) Then
                                .Style.Font = fntHint
                                .Style.BackColor = Color.White
                                .Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                                strTemp = objField.m_aryChars(X, Y)
                            ElseIf (objField.m_tblChars(valTemp) <> "") Then
                                .Style.Font = fntHint
                                .Style.BackColor = Color.White
                                .Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                                strTemp = objField.m_tblChars(valTemp)
                            Else
                                .Style.Font = fntNumber
                                .Style.BackColor = Color.White
                                .Style.Alignment = DataGridViewContentAlignment.TopLeft
                                strTemp = valTemp.ToString()
                            End If

                            If (m_curSelNumber = valTemp) Then
                                .Style.BackColor = Color.Green
                            End If

                            .Value = strTemp
                        End With
                    Next
                End With
            Next
        End With
    End Sub

    Private Sub MainForm_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize

        With Me
            Dim cltWidth As Integer = .Width
            Dim cltHeight As Integer = .Height - .mnuMain.Height - 32

            With .grdField
                .Width = cltWidth - 48
                .Height = cltHeight - 48
                .Top = (cltHeight - .Height) \ 2 + mnuMain.Height
                .Left = (cltWidth - .Width) \ 2
            End With
        End With
    End Sub

    Private Sub grdField_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdField.CellClick
        Dim xSelCol As Integer, ySelRow As Integer

        ' 選択したセルを取得する
        With grdField
            If (.CurrentCell Is Nothing) Then
                m_curSelNumber = PuzzleField.SquareType.FIELD_SQRTYPE_NOSELECT
                Exit Sub
            End If

            With .CurrentCell
                xSelCol = .ColumnIndex
                ySelRow = .RowIndex
            End With
        End With

        Dim nSelNumber As Integer = m_objField.m_aryField(xSelCol, ySelRow)
        If (nSelNumber > 0) Then
            m_curSelNumber = nSelNumber
        End If
        updateField(m_objField)

        If (nSelNumber <= 0) Or (m_frmCheckSheet Is Nothing) Then
            Exit Sub
        End If
        m_frmCheckSheet.changeSelectNumber(nSelNumber)

    End Sub

    Private Sub grdField_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdField.CellDoubleClick
        Dim xSelCol As Integer, ySelRow As Integer

        ' 選択したセルを取得する
        With grdField
            If (.CurrentCell Is Nothing) Then
                m_curSelNumber = PuzzleField.SquareType.FIELD_SQRTYPE_NOSELECT
                Exit Sub
            End If

            With .CurrentCell
                xSelCol = .ColumnIndex
                ySelRow = .RowIndex
            End With
        End With

        Dim objField As PuzzleField = m_objField
        Dim nSelNumber As Integer = objField.m_aryField(xSelCol, ySelRow)
        If (nSelNumber > 0) Then
            m_curSelNumber = nSelNumber
        Else
        End If
        updateField(m_objField)

        If (nSelNumber <= 0) Then
            Exit Sub
        End If
        If m_frmCheckSheet IsNot Nothing Then
            m_frmCheckSheet.changeSelectNumber(nSelNumber)
        End If

        ' 文字を入力する
        inputCharacter(objField, m_frmCheckSheet, nSelNumber)
    End Sub

    Private Sub mnuFileExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileExit.Click

        If MsgBox("終了します", MsgBoxStyle.OkCancel) = MsgBoxResult.Ok Then
            Me.Close()
            End
        End If
    End Sub


    Private Sub mnuFileOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileOpen.Click

        With dlgOpenFile
            .InitialDirectory = ""
            .Filter = "すべてのファイル(*.*)|*.*"
            .FilterIndex = 1
        End With

        If dlgOpenFile.ShowDialog() = DialogResult.Cancel Then
            Exit Sub
        End If

        m_objField = New PuzzleField
        If (m_objField.readFieldFromFile(dlgOpenFile.FileName) = False) Then
            Exit Sub
        End If

        initializeField(m_objField)
        updateField(m_objField)
        mnuFileSave.Enabled = True
        mnuViewCheck.Enabled = True
        showCheckSheet()

    End Sub

    Private Sub mnuFileSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileSave.Click

        With dlgSaveFile
            .InitialDirectory = ""
            .Filter = "すべてのファイル(*.*)|*.*"
            .FilterIndex = 1
        End With

        If dlgSaveFile.ShowDialog() = DialogResult.Cancel Then
            Exit Sub
        End If

        If (m_objField.writeFieldToFile(dlgSaveFile.FileName) = False) Then
            Exit Sub
        End If
        MsgBox("セーブ完了。")

    End Sub

    Private Sub mnuViewCheck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewCheck.Click
        showCheckSheet()
    End Sub

End Class
