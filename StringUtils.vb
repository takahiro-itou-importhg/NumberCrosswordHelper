﻿Module StringUtils

    Public Function getNextToken(ByRef lpTextInOut As String, ByVal strSep As String) As String
        Dim posSep = InStr(lpTextInOut, strSep)

        If (posSep = 0) Then
            ' 区切り文字が見つからない。
            getNextToken = lpTextInOut
            lpTextInOut = ""
            Exit Function
        End If
        If (posSep = 1) Then
            ' 区切り文字が先頭にある。
            lpTextInOut = Mid(lpTextInOut, 2)
            Return ""
        End If

        ' 区切り文字で区切って返す。
        getNextToken = Left(lpTextInOut, posSep - 1)
        lpTextInOut = Mid(lpTextInOut, posSep + 1)
    End Function

End Module
